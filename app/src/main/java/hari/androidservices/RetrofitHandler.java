package hari.androidservices;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Hari on 07/02/16.
 */
public class RetrofitHandler {
    private static String IP_API_BASE_URL = "http://ip-api.com";
    private static RetrofitHandler ourInstance = new RetrofitHandler();

    public static RetrofitHandler getInstance() {
        return ourInstance;
    }

    private RetrofitHandler() {
    }

    //retrofits
    private Retrofit ipApiRetrofit = new Retrofit.Builder()
            .baseUrl(IP_API_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    IpApiRetrofitService ipApiRetrofitService = ipApiRetrofit.create(IpApiRetrofitService.class);


    //API services
    public Call<IpApiResponseModel> getLocationInfo() {
        return ipApiRetrofitService.getLocationInfo();
    }
}
