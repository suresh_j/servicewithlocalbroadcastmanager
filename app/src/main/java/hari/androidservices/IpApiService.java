package hari.androidservices;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Hari on 07/02/16.
 */
public class IpApiService extends IntentService {
    private static final String TAG = IpApiService.class.getSimpleName();
    public static final String IP_API_SERVICE = "hari.androidservices.IP_API_SERVICE";

    public IpApiService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent.getAction().equals(IP_API_SERVICE)) {
            getLocationInfo();
        }
    }

    public static void startIpApiService(Context context) {
        Intent intent = new Intent(context, IpApiService.class);
        intent.setAction(IP_API_SERVICE);
        context.startService(intent);
    }

    private void getLocationInfo() {
        RetrofitHandler.getInstance().getLocationInfo().enqueue(new Callback<IpApiResponseModel>() {
            @Override
            public void onResponse(Call<IpApiResponseModel> call, Response<IpApiResponseModel> response) {
                if (response.isSuccess()) {
                    Log.e(TAG, "Successfully got from Ip Api");
                    sendBroadcast(IP_API_SERVICE, true, response.message(), response.body().toJson().toString());
                } else {
                    sendBroadcast(IP_API_SERVICE, false, response.message(), null);
                    Log.e(TAG, "Failed to get location info from Ip Api");
                }
            }

            @Override
            public void onFailure(Call<IpApiResponseModel> call, Throwable t) {
                sendBroadcast(IP_API_SERVICE, false, t.getMessage(), null);
                Log.e(TAG, "Failed to get location info from Ip Api");
            }
        });
    }

    private void sendBroadcast(String action, boolean error, String errorMessage, String results) {
        Log.e(TAG, "sendBroadcast action: " + action + ", error: " + error + " errorMessage: " + errorMessage);
        Intent localIntent = new Intent(action);
        localIntent.putExtra(Constants.ERROR, error);
        if (error) {
            localIntent.putExtra(Constants.RESULT, results);
        }
        // Broadcasts the Intent to receivers in this app.
        LocalBroadcastManager.getInstance(IpApiService.this).sendBroadcast(localIntent);
    }
}
