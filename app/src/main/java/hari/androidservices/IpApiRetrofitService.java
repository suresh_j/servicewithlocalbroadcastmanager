package hari.androidservices;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Hari on 07/02/16.
 */
public interface IpApiRetrofitService {
    @GET("/json")
    Call<IpApiResponseModel> getLocationInfo();
}
